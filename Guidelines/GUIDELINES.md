## Fixing various problems with VMware modules not working properly
This file explains some problems you can encounter while working with VMware Workstation and how to solve them. Basically, it's just some fixes for your system that can be both be temporary or permanent. If the temporary solution provided here can be done permanently, do a pull request. I will add all the solutions for the problems i encountered while using VMware. If you do have a solution for your concrete problem, it can be added to this guide as well.

### Problem №1
##### Could not connect Ethernet0 to any of the VMnet interfaces
This problem is rare to encounter, but tough to locate at first glance. The service provided by the repository itself may be working (can be checked by running "ps -aux | grep vmnet8") out of the box, but can't do anything as the Virtual Network interface can't just be read.
When trying to connect any of the interfaces, the following window should pop out:

![](https://codeberg.org/Chaimann/vmware-systemd-services/raw/commit/7aad91907d060b5c5be8dbdc032150677d6c10ed/Guidelines/Assets/Image1.png "The mentioned window")

This can happen after updating the kernel and booting the previous version back. The reason is simple: the module you are trying to load requires the kernel it was compiled for.
If the problem persists, you can force the interfaces to start by typing "sudo vmware-networks --start". If everything's done correctly, you're gonna get the next output:

![](https://codeberg.org/Chaimann/vmware-systemd-services/raw/branch/default/Guidelines/Assets/Image2.png)

You can double-check the status by looking on the top-right menu: (for GNOME Desktop users only)

![](https://codeberg.org/Chaimann/vmware-systemd-services/raw/branch/default/Guidelines/Assets/Image3.png)

Unfortunately, that's a temporary solution until reboot. Fortunately, it solves everything and does not crash your system. What's even better, i'm experimenting with this command in order to make it a self-starting systemd service and put it to a new repository as it won't be a part of a core services that are bundled with this exact repo.

### More to come.
##### Stay tuned...