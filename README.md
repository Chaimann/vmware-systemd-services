Today, i proudly present you...
# vmware-systemd-services
## For those who want to get rid of that annoying "no such file or directory". :D

A simple repository for containing all the basic needles for VMware Workstation to work on Linux hosts. Useful for those who use newer kernels and systemd init system.

By default, VMware can't really operate with systemd, as it doesn't have any kind of "rc.d"-stylized scripts, which can be a real pain for someone. Well, to make it work, you just have to compile the modules, install the scripts listed in that directory, and you're good to go!


###### Why does the process take so long?
Well, there's a ten-second pause for user to make sure if he executed the script by root (or with sudo). I am too lazy to write a user checker, so i'm counting on you... :D


###### Have you tested it?
Yes. I'm gonna admit, i'm using these services myself.


###### Are you going to add more services?
Sure. I'm still learning how other services work, and how are they triggered on the host.
_Turns out those services that i made were using modprobe_


So... Over the last few weeks i've been experiencing some weird issues with VMNET. The same thing may happen with your system. In order to get everything working again, i've prepared a **[special guide](https://codeberg.org/Chaimann/vmware-systemd-services/src/branch/default/Guidelines/GUIDELINES.md#problem-1)** that will probably help you as well.