#!/bin/sh

echo 'This script will copy all the services at default systemd system scripts directory, so it should be executed as root (with sudo). If you are unsure that you are running this script with sudo, press "Ctrl+C" type "sudo !!", otherwise, wait for 10 seconds.'
sleep 10

# copy all the services at the scripts directory
cp vmware-vmmon-chai.service /etc/systemd/system/vmware-vmmon-chai.service
cp vmware-vmnet-chai.service /etc/systemd/system/vmware-vmnet-chai.service
cp vmware-vmci-chai.service /etc/systemd/system/vmware-vmci-chai.service
cp vmware-usbarb-chai.service /etc/systemd/system/vmware-usbarb-chai.service

# enable them
systemctl enable vmware-vmnet-chai.service
systemctl enable vmware-vmmon-chai.service
systemctl enable vmware-vmci-chai.service
systemctl enable vmware-usbarb-chai.service

# reload
systemctl daemon-reload

# end
echo "All the needles are installed and enabled. Restart your computer to make them work."
