#!/bin/sh

echo 'This script will remove all the services from the default systemd system scripts directory, so it should be executed as root (with sudo). If you are unsure that you are running this script with sudo, press "Ctrl+C" type "sudo !!", otherwise, wait for 10 seconds.'
sleep 10

# disable services before deleting them
systemctl disable vmware-vmnet-chai.service
systemctl disable vmware-vmmon-chai.service
systemctl disable vmware-vmci-chai.service
systemctl disable vmware-usbarb-chai.service

# and then remove them
rm /etc/systemd/system/vmware-vmmon-chai.service
rm /etc/systemd/system/vmware-vmnet-chai.service
rm /etc/systemd/system/vmware-vmci-chai.service
rm /etc/systemd/system/vmware-usbarb-chai.service

# reload
systemctl daemon-reload

# end
echo "All the modules are now disabled and deleted. Restart your computer."
